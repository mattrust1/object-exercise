public class Person {

    //attributes
    private String firstName;
    private String surname;
    private Pet myPet;

    public Person(String fName, String sName){

        //constructor
        this.firstName = fName;
        this.surname = sName;
    }

    // methods
    public String getFirstName(){

        return firstName;
    }
    public String getSurname(){

        return  surname;
    }
    public String getFullName(){

        return  firstName + " " + surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }
    public  void setFirstName(String firstName){

        this.firstName = firstName;
    }
    public void setMyPet(Pet myPet){
        this.myPet = myPet;
    }
    public Pet getMyPet(){
        return myPet;
    }
}
